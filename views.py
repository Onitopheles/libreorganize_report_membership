from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import get_template

from tendenci.apps.memberships.models import MembershipDefault

from weasyprint import HTML


@login_required
def members_pdf(
        request,
        template_name='report/members_pdf.html',
        static_logo='report/logo.png',
):
    try:
        show_all = bool(int(request.GET.get('show_all', '0')))
    except:
        show_all = False 
    response = HttpResponse(content_type='application/pdf')
    template = get_template(template_name)
    members = []
    memberships = MembershipDefault.objects.filter(
        status_detail='active'
    ).order_by(
        'user__demographics__ud2',
        'user__last_name',
        'user__first_name',
    )
    for membership in memberships:
        user = membership.user
        demographics = user.demographics
        if show_all or (not show_all and demographics.ud4):
            members.append({
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'state': demographics.ud2 or '',
                'id_number': demographics.ud4 or '',
            })
    context = RequestContext(
        request, {
            'logo_url': request.build_absolute_uri(static(static_logo)),
            'members': members,
        }
    )
    html = template.render(context)
    response.write(HTML(string=html).write_pdf())
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(
        'members.pdf'
    )
    return response
