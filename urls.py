from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'reports/members_pdf/$',
        views.members_pdf,
        name='report.members_pdf',
    ),
]
